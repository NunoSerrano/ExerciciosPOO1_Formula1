﻿namespace ExerciciosPOO1_Formula1
{
    partial class form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupBoxSettings = new System.Windows.Forms.GroupBox();
            this.LabelCustoLitro = new System.Windows.Forms.Label();
            this.TextBoxCustoLitro = new System.Windows.Forms.TextBox();
            this.LabelCustoLitroText = new System.Windows.Forms.Label();
            this.LabelDepositoCaract = new System.Windows.Forms.Label();
            this.LabelConsumo100Caract = new System.Windows.Forms.Label();
            this.LabelMatriculaCaract = new System.Windows.Forms.Label();
            this.LabelConsumo100 = new System.Windows.Forms.Label();
            this.TextBoxConsumo100 = new System.Windows.Forms.TextBox();
            this.TextBoxDeposito = new System.Windows.Forms.TextBox();
            this.LabelDeposito = new System.Windows.Forms.Label();
            this.ButtonSettingsOK = new System.Windows.Forms.Button();
            this.TextBoxMatricula = new System.Windows.Forms.TextBox();
            this.LabelMatricula = new System.Windows.Forms.Label();
            this.LabelReserva = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.LabelCustoKm = new System.Windows.Forms.Label();
            this.LabelAutonomia = new System.Windows.Forms.Label();
            this.AutonomiaKmText = new System.Windows.Forms.Label();
            this.LabelCustoKmText = new System.Windows.Forms.Label();
            this.ButtonEncher = new System.Windows.Forms.Button();
            this.ButtonAtestar = new System.Windows.Forms.Button();
            this.LabelCombustivel = new System.Windows.Forms.Label();
            this.LabelCombustivelText = new System.Windows.Forms.Label();
            this.GroupBoxViagens = new System.Windows.Forms.GroupBox();
            this.DataGridViewViagens = new System.Windows.Forms.DataGridView();
            this.NumeroViagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KmViagem = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumericUpDownViagem = new System.Windows.Forms.NumericUpDown();
            this.LabelViagemNumero = new System.Windows.Forms.Label();
            this.ButtonIniciarViagem = new System.Windows.Forms.Button();
            this.LabelViagemText = new System.Windows.Forms.Label();
            this.LabelKm = new System.Windows.Forms.Label();
            this.LabelKmPercorridos = new System.Windows.Forms.Label();
            this.GroupBoxSettings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.GroupBoxViagens.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewViagens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownViagem)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupBoxSettings
            // 
            this.GroupBoxSettings.Controls.Add(this.LabelCustoLitro);
            this.GroupBoxSettings.Controls.Add(this.TextBoxCustoLitro);
            this.GroupBoxSettings.Controls.Add(this.LabelCustoLitroText);
            this.GroupBoxSettings.Controls.Add(this.LabelDepositoCaract);
            this.GroupBoxSettings.Controls.Add(this.LabelConsumo100Caract);
            this.GroupBoxSettings.Controls.Add(this.LabelMatriculaCaract);
            this.GroupBoxSettings.Controls.Add(this.LabelConsumo100);
            this.GroupBoxSettings.Controls.Add(this.TextBoxConsumo100);
            this.GroupBoxSettings.Controls.Add(this.TextBoxDeposito);
            this.GroupBoxSettings.Controls.Add(this.LabelDeposito);
            this.GroupBoxSettings.Controls.Add(this.ButtonSettingsOK);
            this.GroupBoxSettings.Controls.Add(this.TextBoxMatricula);
            this.GroupBoxSettings.Controls.Add(this.LabelMatricula);
            this.GroupBoxSettings.Location = new System.Drawing.Point(12, 21);
            this.GroupBoxSettings.Name = "GroupBoxSettings";
            this.GroupBoxSettings.Size = new System.Drawing.Size(417, 377);
            this.GroupBoxSettings.TabIndex = 0;
            this.GroupBoxSettings.TabStop = false;
            this.GroupBoxSettings.Text = "Settings";
            // 
            // LabelCustoLitro
            // 
            this.LabelCustoLitro.AutoSize = true;
            this.LabelCustoLitro.Location = new System.Drawing.Point(163, 176);
            this.LabelCustoLitro.Name = "LabelCustoLitro";
            this.LabelCustoLitro.Size = new System.Drawing.Size(19, 13);
            this.LabelCustoLitro.TabIndex = 12;
            this.LabelCustoLitro.Text = "- - ";
            // 
            // TextBoxCustoLitro
            // 
            this.TextBoxCustoLitro.Location = new System.Drawing.Point(280, 170);
            this.TextBoxCustoLitro.Name = "TextBoxCustoLitro";
            this.TextBoxCustoLitro.Size = new System.Drawing.Size(100, 20);
            this.TextBoxCustoLitro.TabIndex = 10;
            // 
            // LabelCustoLitroText
            // 
            this.LabelCustoLitroText.AutoSize = true;
            this.LabelCustoLitroText.Location = new System.Drawing.Point(6, 173);
            this.LabelCustoLitroText.Name = "LabelCustoLitroText";
            this.LabelCustoLitroText.Size = new System.Drawing.Size(131, 13);
            this.LabelCustoLitroText.TabIndex = 11;
            this.LabelCustoLitroText.Text = "Custo do Combustível €/L";
            // 
            // LabelDepositoCaract
            // 
            this.LabelDepositoCaract.AutoSize = true;
            this.LabelDepositoCaract.Location = new System.Drawing.Point(163, 79);
            this.LabelDepositoCaract.Name = "LabelDepositoCaract";
            this.LabelDepositoCaract.Size = new System.Drawing.Size(19, 13);
            this.LabelDepositoCaract.TabIndex = 10;
            this.LabelDepositoCaract.Text = "- - ";
            // 
            // LabelConsumo100Caract
            // 
            this.LabelConsumo100Caract.AutoSize = true;
            this.LabelConsumo100Caract.Location = new System.Drawing.Point(163, 126);
            this.LabelConsumo100Caract.Name = "LabelConsumo100Caract";
            this.LabelConsumo100Caract.Size = new System.Drawing.Size(16, 13);
            this.LabelConsumo100Caract.TabIndex = 9;
            this.LabelConsumo100Caract.Text = "- -";
            // 
            // LabelMatriculaCaract
            // 
            this.LabelMatriculaCaract.AutoSize = true;
            this.LabelMatriculaCaract.Location = new System.Drawing.Point(163, 30);
            this.LabelMatriculaCaract.Name = "LabelMatriculaCaract";
            this.LabelMatriculaCaract.Size = new System.Drawing.Size(16, 13);
            this.LabelMatriculaCaract.TabIndex = 8;
            this.LabelMatriculaCaract.Text = "- -";
            // 
            // LabelConsumo100
            // 
            this.LabelConsumo100.AutoSize = true;
            this.LabelConsumo100.Location = new System.Drawing.Point(6, 129);
            this.LabelConsumo100.Name = "LabelConsumo100";
            this.LabelConsumo100.Size = new System.Drawing.Size(98, 13);
            this.LabelConsumo100.TabIndex = 1;
            this.LabelConsumo100.Text = "Consumo L/100Km";
            // 
            // TextBoxConsumo100
            // 
            this.TextBoxConsumo100.Location = new System.Drawing.Point(280, 120);
            this.TextBoxConsumo100.Name = "TextBoxConsumo100";
            this.TextBoxConsumo100.Size = new System.Drawing.Size(100, 20);
            this.TextBoxConsumo100.TabIndex = 7;
            // 
            // TextBoxDeposito
            // 
            this.TextBoxDeposito.Location = new System.Drawing.Point(280, 69);
            this.TextBoxDeposito.Name = "TextBoxDeposito";
            this.TextBoxDeposito.Size = new System.Drawing.Size(100, 20);
            this.TextBoxDeposito.TabIndex = 4;
            // 
            // LabelDeposito
            // 
            this.LabelDeposito.AutoSize = true;
            this.LabelDeposito.Location = new System.Drawing.Point(6, 75);
            this.LabelDeposito.Name = "LabelDeposito";
            this.LabelDeposito.Size = new System.Drawing.Size(64, 13);
            this.LabelDeposito.TabIndex = 1;
            this.LabelDeposito.Text = "Deposito (L)";
            // 
            // ButtonSettingsOK
            // 
            this.ButtonSettingsOK.Location = new System.Drawing.Point(280, 224);
            this.ButtonSettingsOK.Name = "ButtonSettingsOK";
            this.ButtonSettingsOK.Size = new System.Drawing.Size(100, 23);
            this.ButtonSettingsOK.TabIndex = 3;
            this.ButtonSettingsOK.Text = "Alterar Settings";
            this.ButtonSettingsOK.UseVisualStyleBackColor = true;
            this.ButtonSettingsOK.Click += new System.EventHandler(this.ButtonSettingsOK_Click);
            // 
            // TextBoxMatricula
            // 
            this.TextBoxMatricula.Location = new System.Drawing.Point(280, 24);
            this.TextBoxMatricula.Name = "TextBoxMatricula";
            this.TextBoxMatricula.Size = new System.Drawing.Size(100, 20);
            this.TextBoxMatricula.TabIndex = 2;
            // 
            // LabelMatricula
            // 
            this.LabelMatricula.AutoSize = true;
            this.LabelMatricula.Location = new System.Drawing.Point(6, 30);
            this.LabelMatricula.Name = "LabelMatricula";
            this.LabelMatricula.Size = new System.Drawing.Size(50, 13);
            this.LabelMatricula.TabIndex = 1;
            this.LabelMatricula.Text = "Matricula";
            // 
            // LabelReserva
            // 
            this.LabelReserva.AutoSize = true;
            this.LabelReserva.BackColor = System.Drawing.Color.Orange;
            this.LabelReserva.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelReserva.Location = new System.Drawing.Point(168, 28);
            this.LabelReserva.Name = "LabelReserva";
            this.LabelReserva.Size = new System.Drawing.Size(67, 16);
            this.LabelReserva.TabIndex = 5;
            this.LabelReserva.Text = "Reserva";
            this.LabelReserva.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.LabelCustoKm);
            this.groupBox2.Controls.Add(this.LabelAutonomia);
            this.groupBox2.Controls.Add(this.AutonomiaKmText);
            this.groupBox2.Controls.Add(this.LabelCustoKmText);
            this.groupBox2.Controls.Add(this.ButtonEncher);
            this.groupBox2.Controls.Add(this.ButtonAtestar);
            this.groupBox2.Controls.Add(this.LabelCombustivel);
            this.groupBox2.Controls.Add(this.LabelCombustivelText);
            this.groupBox2.Controls.Add(this.LabelReserva);
            this.groupBox2.Location = new System.Drawing.Point(486, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(241, 377);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Combustivel";
            // 
            // LabelCustoKm
            // 
            this.LabelCustoKm.AutoSize = true;
            this.LabelCustoKm.Location = new System.Drawing.Point(115, 195);
            this.LabelCustoKm.Name = "LabelCustoKm";
            this.LabelCustoKm.Size = new System.Drawing.Size(13, 13);
            this.LabelCustoKm.TabIndex = 10;
            this.LabelCustoKm.Text = "0";
            // 
            // LabelAutonomia
            // 
            this.LabelAutonomia.AutoSize = true;
            this.LabelAutonomia.Location = new System.Drawing.Point(115, 146);
            this.LabelAutonomia.Name = "LabelAutonomia";
            this.LabelAutonomia.Size = new System.Drawing.Size(13, 13);
            this.LabelAutonomia.TabIndex = 15;
            this.LabelAutonomia.Text = "0";
            // 
            // AutonomiaKmText
            // 
            this.AutonomiaKmText.AutoSize = true;
            this.AutonomiaKmText.Location = new System.Drawing.Point(18, 145);
            this.AutonomiaKmText.Name = "AutonomiaKmText";
            this.AutonomiaKmText.Size = new System.Drawing.Size(75, 13);
            this.AutonomiaKmText.TabIndex = 16;
            this.AutonomiaKmText.Text = "Autonomia Km";
            // 
            // LabelCustoKmText
            // 
            this.LabelCustoKmText.AutoSize = true;
            this.LabelCustoKmText.Location = new System.Drawing.Point(18, 195);
            this.LabelCustoKmText.Name = "LabelCustoKmText";
            this.LabelCustoKmText.Size = new System.Drawing.Size(54, 13);
            this.LabelCustoKmText.TabIndex = 17;
            this.LabelCustoKmText.Text = "Custo/Km";
            // 
            // ButtonEncher
            // 
            this.ButtonEncher.Location = new System.Drawing.Point(21, 92);
            this.ButtonEncher.Name = "ButtonEncher";
            this.ButtonEncher.Size = new System.Drawing.Size(75, 23);
            this.ButtonEncher.TabIndex = 10;
            this.ButtonEncher.Text = "Encher +";
            this.ButtonEncher.UseVisualStyleBackColor = true;
            this.ButtonEncher.Click += new System.EventHandler(this.ButtonEncher_Click);
            // 
            // ButtonAtestar
            // 
            this.ButtonAtestar.Location = new System.Drawing.Point(160, 92);
            this.ButtonAtestar.Name = "ButtonAtestar";
            this.ButtonAtestar.Size = new System.Drawing.Size(75, 23);
            this.ButtonAtestar.TabIndex = 10;
            this.ButtonAtestar.Text = "Atestar";
            this.ButtonAtestar.UseVisualStyleBackColor = true;
            this.ButtonAtestar.Click += new System.EventHandler(this.ButtonAtestar_Click);
            // 
            // LabelCombustivel
            // 
            this.LabelCombustivel.AutoSize = true;
            this.LabelCombustivel.Location = new System.Drawing.Point(118, 30);
            this.LabelCombustivel.Name = "LabelCombustivel";
            this.LabelCombustivel.Size = new System.Drawing.Size(13, 13);
            this.LabelCombustivel.TabIndex = 10;
            this.LabelCombustivel.Text = "0";
            // 
            // LabelCombustivelText
            // 
            this.LabelCombustivelText.AutoSize = true;
            this.LabelCombustivelText.Location = new System.Drawing.Point(18, 30);
            this.LabelCombustivelText.Name = "LabelCombustivelText";
            this.LabelCombustivelText.Size = new System.Drawing.Size(81, 13);
            this.LabelCombustivelText.TabIndex = 10;
            this.LabelCombustivelText.Text = "Combustível (L)";
            // 
            // GroupBoxViagens
            // 
            this.GroupBoxViagens.Controls.Add(this.DataGridViewViagens);
            this.GroupBoxViagens.Controls.Add(this.NumericUpDownViagem);
            this.GroupBoxViagens.Controls.Add(this.LabelViagemNumero);
            this.GroupBoxViagens.Controls.Add(this.ButtonIniciarViagem);
            this.GroupBoxViagens.Controls.Add(this.LabelViagemText);
            this.GroupBoxViagens.Controls.Add(this.LabelKm);
            this.GroupBoxViagens.Controls.Add(this.LabelKmPercorridos);
            this.GroupBoxViagens.Location = new System.Drawing.Point(745, 21);
            this.GroupBoxViagens.Name = "GroupBoxViagens";
            this.GroupBoxViagens.Size = new System.Drawing.Size(409, 377);
            this.GroupBoxViagens.TabIndex = 9;
            this.GroupBoxViagens.TabStop = false;
            this.GroupBoxViagens.Text = "Viagens";
            // 
            // DataGridViewViagens
            // 
            this.DataGridViewViagens.AllowUserToAddRows = false;
            this.DataGridViewViagens.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridViewViagens.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NumeroViagem,
            this.KmViagem});
            this.DataGridViewViagens.Location = new System.Drawing.Point(65, 161);
            this.DataGridViewViagens.Name = "DataGridViewViagens";
            this.DataGridViewViagens.ReadOnly = true;
            this.DataGridViewViagens.Size = new System.Drawing.Size(245, 216);
            this.DataGridViewViagens.TabIndex = 16;
            // 
            // NumeroViagem
            // 
            this.NumeroViagem.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NumeroViagem.HeaderText = "Viagem";
            this.NumeroViagem.Name = "NumeroViagem";
            this.NumeroViagem.ReadOnly = true;
            // 
            // KmViagem
            // 
            this.KmViagem.HeaderText = "Km";
            this.KmViagem.Name = "KmViagem";
            this.KmViagem.ReadOnly = true;
            // 
            // NumericUpDownViagem
            // 
            this.NumericUpDownViagem.Location = new System.Drawing.Point(95, 70);
            this.NumericUpDownViagem.Maximum = new decimal(new int[] {
            10000000,
            0,
            0,
            0});
            this.NumericUpDownViagem.Name = "NumericUpDownViagem";
            this.NumericUpDownViagem.Size = new System.Drawing.Size(120, 20);
            this.NumericUpDownViagem.TabIndex = 15;
            // 
            // LabelViagemNumero
            // 
            this.LabelViagemNumero.AutoSize = true;
            this.LabelViagemNumero.Location = new System.Drawing.Point(62, 75);
            this.LabelViagemNumero.Name = "LabelViagemNumero";
            this.LabelViagemNumero.Size = new System.Drawing.Size(13, 13);
            this.LabelViagemNumero.TabIndex = 14;
            this.LabelViagemNumero.Text = "1";
            // 
            // ButtonIniciarViagem
            // 
            this.ButtonIniciarViagem.Location = new System.Drawing.Point(95, 116);
            this.ButtonIniciarViagem.Name = "ButtonIniciarViagem";
            this.ButtonIniciarViagem.Size = new System.Drawing.Size(120, 23);
            this.ButtonIniciarViagem.TabIndex = 10;
            this.ButtonIniciarViagem.Text = "Iniciar Viagem";
            this.ButtonIniciarViagem.UseVisualStyleBackColor = true;
            this.ButtonIniciarViagem.Click += new System.EventHandler(this.ButtonIniciarViagem_Click);
            // 
            // LabelViagemText
            // 
            this.LabelViagemText.AutoSize = true;
            this.LabelViagemText.Location = new System.Drawing.Point(19, 75);
            this.LabelViagemText.Name = "LabelViagemText";
            this.LabelViagemText.Size = new System.Drawing.Size(42, 13);
            this.LabelViagemText.TabIndex = 13;
            this.LabelViagemText.Text = "Viagem";
            // 
            // LabelKm
            // 
            this.LabelKm.AutoSize = true;
            this.LabelKm.Location = new System.Drawing.Point(138, 30);
            this.LabelKm.Name = "LabelKm";
            this.LabelKm.Size = new System.Drawing.Size(13, 13);
            this.LabelKm.TabIndex = 12;
            this.LabelKm.Text = "0";
            // 
            // LabelKmPercorridos
            // 
            this.LabelKmPercorridos.AutoSize = true;
            this.LabelKmPercorridos.Location = new System.Drawing.Point(19, 30);
            this.LabelKmPercorridos.Name = "LabelKmPercorridos";
            this.LabelKmPercorridos.Size = new System.Drawing.Size(78, 13);
            this.LabelKmPercorridos.TabIndex = 11;
            this.LabelKmPercorridos.Text = "Km Percorridos";
            // 
            // form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1166, 410);
            this.Controls.Add(this.GroupBoxViagens);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.GroupBoxSettings);
            this.Name = "form1";
            this.Text = "Formula1";
            this.GroupBoxSettings.ResumeLayout(false);
            this.GroupBoxSettings.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.GroupBoxViagens.ResumeLayout(false);
            this.GroupBoxViagens.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridViewViagens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumericUpDownViagem)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupBoxSettings;
        private System.Windows.Forms.TextBox TextBoxMatricula;
        private System.Windows.Forms.Label LabelMatricula;
        private System.Windows.Forms.Button ButtonSettingsOK;
        private System.Windows.Forms.Label LabelConsumo100;
        private System.Windows.Forms.TextBox TextBoxConsumo100;
        private System.Windows.Forms.TextBox TextBoxDeposito;
        private System.Windows.Forms.Label LabelDeposito;
        private System.Windows.Forms.Label LabelReserva;
        private System.Windows.Forms.Label LabelMatriculaCaract;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox GroupBoxViagens;
        private System.Windows.Forms.Label LabelDepositoCaract;
        private System.Windows.Forms.Label LabelConsumo100Caract;
        private System.Windows.Forms.Button ButtonAtestar;
        private System.Windows.Forms.Label LabelCombustivel;
        private System.Windows.Forms.Label LabelCombustivelText;
        private System.Windows.Forms.Button ButtonEncher;
        private System.Windows.Forms.Label LabelKm;
        private System.Windows.Forms.Label LabelKmPercorridos;
        private System.Windows.Forms.Label LabelCustoKm;
        private System.Windows.Forms.Label LabelAutonomia;
        private System.Windows.Forms.Label AutonomiaKmText;
        private System.Windows.Forms.Label LabelCustoKmText;
        private System.Windows.Forms.Label LabelViagemNumero;
        private System.Windows.Forms.Button ButtonIniciarViagem;
        private System.Windows.Forms.Label LabelViagemText;
        private System.Windows.Forms.Label LabelCustoLitroText;
        private System.Windows.Forms.Label LabelCustoLitro;
        private System.Windows.Forms.TextBox TextBoxCustoLitro;
        private System.Windows.Forms.NumericUpDown NumericUpDownViagem;
        private System.Windows.Forms.DataGridView DataGridViewViagens;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumeroViagem;
        private System.Windows.Forms.DataGridViewTextBoxColumn KmViagem;
    }
}

