﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ExerciciosPOO1_Formula1
{
    public partial class form1 : Form
    {

        public Formula1 ferrari;

        double custoLitro = 1.5;

        int numeroViagem = 1;

        public form1()
        {
            InitializeComponent();

            ferrari = new Formula1();


            LabelMatriculaCaract.Text = ferrari.Matricula;
            LabelDepositoCaract.Text = ferrari.Deposito.ToString();
            LabelConsumo100Caract.Text = ferrari.Consumo100.ToString();

            LabelReserva.Visible = ferrari.AcenderReserva();

            
            LabelCustoLitro.Text = custoLitro.ToString();


            

        }



        private void ButtonSettingsOK_Click(object sender, EventArgs e)

        {
            ferrari.Matricula = TextBoxMatricula.Text;


            double a = 0, b=0;
     

            if (Double.TryParse(TextBoxDeposito.Text, out a) )
                {
                ferrari.Deposito = a;
            }

            if (Double.TryParse(TextBoxConsumo100.Text, out a))
            {
                ferrari.Consumo100 = a;
            }


            if (Double.TryParse(TextBoxCustoLitro.Text, out a))
            {
                custoLitro = a;
            }

            LabelMatriculaCaract.Text = ferrari.Matricula;
            LabelDepositoCaract.Text = ferrari.Deposito.ToString();
            LabelConsumo100Caract.Text = ferrari.Consumo100.ToString();
            LabelCustoLitro.Text = LabelCustoLitro.ToString();

            TextBoxMatricula.Text = null;
            TextBoxDeposito.Text = null;
            TextBoxConsumo100.Text = null;

           
        }

       
        private void ButtonAtestar_Click(object sender, EventArgs e)
        {
            ferrari.Atestar();
            LabelCombustivel.Text = ferrari.NivelCombustivel.ToString();
            LabelReserva.Visible = ferrari.AcenderReserva();
            LabelAutonomia.Text = ferrari.Autonomia().ToString();
            LabelCustoKm.Text = ferrari.CustoKm(custoLitro).ToString();
        }

        private void ButtonEncher_Click(object sender, EventArgs e)
        {
            // forma de simplificar o If
            ferrari.NivelCombustivel = (ferrari.NivelCombustivel >= ferrari.Deposito) ? ferrari.Deposito : ferrari.NivelCombustivel+10;

            LabelCombustivel.Text = ferrari.NivelCombustivel.ToString();
            LabelReserva.Visible = ferrari.AcenderReserva();
            LabelAutonomia.Text = ferrari.Autonomia().ToString();
            LabelCustoKm.Text = ferrari.CustoKm(custoLitro).ToString();
        }

        private void ButtonIniciarViagem_Click(object sender, EventArgs e)
        {
            // EM FALTA
            
            // Falta limitar as viagens quando não existe combustivel suficiente
            // falta por a autonimia sempre a funcionar quando o nivel é actualizado pelas viagens
            // falta colocar mensagem a avisar que não tem combustivel suficiente
            // falta fazer regex as matriculas e outras validações


            LabelKm.Text=  (ferrari.QuilometrosTotais(Convert.ToInt32(NumericUpDownViagem.Value))).ToString();
            
            DataGridViewViagens.Rows.Add(numeroViagem, NumericUpDownViagem.Value); // adicionar linhas com os respectivos dados

            numeroViagem++;

            LabelViagemNumero.Text = numeroViagem.ToString();



            ferrari.NivelCombustivel = ferrari.NivelCombustivel - ferrari.CombustivelGastoNaViagem(Convert.ToInt32(NumericUpDownViagem.Value));

            LabelCombustivel.Text = ferrari.NivelCombustivel.ToString();
        }

        
    }
    }

