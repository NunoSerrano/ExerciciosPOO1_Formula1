﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExerciciosPOO1_Formula1
{
    public class Formula1
    {


        #region Atributos


        private string _matricula;

        private int _quilometros;

        private double _deposito;

        private double _nivelcombustivel;

        private double _consumo100;

        private int _viagem;

        private bool _reserva;

        

        #endregion



        #region Propriedades

        public string Matricula { get; set; }

        public int Quilometros { get; set; }

        public double Deposito { get; set; }


        


        public double NivelCombustivel
        {

            get { return _nivelcombustivel; }

            set
            {
                if (value <= Deposito)
                {
                    _nivelcombustivel = value;
                }

                if (_nivelcombustivel<=0)
                {
                    _nivelcombustivel = 0;
                }


            }
        }


        public double Consumo100 { get; set; }      


        public int Viagem { get; set; }


        public bool Reserva { get; set; }

        #endregion
        #region Construtores


        public Formula1() : this("HZ12345", 0, 100, 0, 10, 0, false) { }



        public Formula1 (Formula1 formula1) : this (formula1._matricula, 
            formula1._quilometros, formula1._deposito, formula1._nivelcombustivel,
            formula1._consumo100, formula1._viagem, formula1._reserva){}


             public Formula1 (string _matricula, int _quilometros, double _depositos, 
                 double _nivelcombustivel,double _consumo100,int _viagem, bool _reserva )
        {
            Matricula= "HZ12345";
            Quilometros = 0;
            Deposito = 100;
            NivelCombustivel = 0;
            Consumo100 = 10;
            Viagem = 0;
            Reserva = false;
            
    }

        #endregion


        #region metodos


        public void SettingsMatricula(string text)
        {
            Matricula = text;
        }


        public void  SettingsDeposito(double value)
        {
            Deposito = value;
        }


        public void SettingsReserva ()
        {
            Reserva = true;
        }


        public void SettingsConsumo100(double value)
        {
           Consumo100 =value ;
        }



        public double Autonomia ()
        {
            double value = NivelCombustivel*100 / Consumo100;

            return value;
        }

        public double CombustivelGastoNaViagem (int value)
        {
            double Combustivel = value / Consumo100;
            return Combustivel;
        }


        public int ContadorViagens(int value)
        {
            int numeroviagem =value;
            return numeroviagem;
        }


        public double QuilometrosTotais(int value)
        {
            Quilometros = Quilometros + value;

            return Quilometros;
        }

        

        public double CustoKm(double valuecustoLitro) 
            {

            double value = (Consumo100 / 100)* valuecustoLitro;

            return value;

            }

        


        public void Atestar()
        {

            NivelCombustivel = Deposito;

        }

        public void Abastecer ()
        {
            NivelCombustivel++;
        }

        public bool AcenderReserva()
        {
            if (NivelCombustivel <= 10)
            {
                Reserva = true;
            }
            else
            {
                Reserva = false;
            }

            return Reserva;
        }


      



        #endregion
    }
}
