﻿namespace Exercicio_POO_4
{
    using System.Text;

    public class Veiculo
    {
        #region Atributos

        private string _matricula;

        private double _kmTotal;

        private double _deposito;

        private double _litros;

        private double _consumoMedio;

        private int _viagens;

        #endregion


        #region Propriedades

        public string Matricula { get; set; }

        public double KmTotal { get; private set; }

        public double Deposito { get; set; }

        public double Litros { get; private set; }

        public double ConsumoMedio { get; set; }

        public int Viagens { get; private set; }

        #endregion

        #region Construtores

        //Construtor por parâmetros
        public Veiculo(double KmIniciais, double dep, double numLitros,
            double consumoMedio, int numViagens, string mat)
        {
            KmTotal = KmIniciais;
            Deposito = dep;
            Litros = numLitros;
            ConsumoMedio = consumoMedio;
            Viagens = numViagens;
            Matricula = mat;
        }


        //Construtor de cópia

        public Veiculo(Veiculo veic)
        {
            Matricula = veic.Matricula;
            KmTotal = veic.KmTotal;
            Deposito = veic.Deposito;
            Litros = veic.Litros;
            ConsumoMedio = veic.ConsumoMedio;
            Viagens = veic.Viagens;
        }
        #endregion

        
        public double Percorrer()
        {
            return Litros / ConsumoMedio * 100;
        }

        /// <summary>
        /// Poderá percorrer mais Km's ????
        /// </summary>
        /// <param name="km"></param>
        /// <returns></returns>
        public bool podePercorrer(double km)
        {
            double vaiGastar = ConsumoMedio / 100 * km;
            return Litros > vaiGastar;
        }

        /// <summary>
        /// Regista uma nova viagem
        /// </summary>
        /// <param name="km"></param>
        public void RegistaViagem(double km)
        {
            double gastou = ConsumoMedio / 100 * km;
            Viagens++;
            KmTotal += km;
            Litros -= gastou;
        }

        /// <summary>
        /// Estará na reserva ??
        /// </summary>
        /// <returns></returns>
        public bool naReserva()
        {
            return Litros <= 10;
        }


        /// <summary>
        /// Total gasto em combustível desde a compra
        /// </summary>
        /// <param name="precoLitro"></param>
        /// <returns></returns>
        public double TotalGastoAteHoje(double precoLitro)
        {
            return ConsumoMedio / 100 * KmTotal * precoLitro;
        }

        /// <summary>
        /// Custo por quilómetro percorrido
        /// </summary>
        /// <param name="precoLitro"></param>
        /// <returns></returns>
        public double custoKm(double precoLitro)
        {
            return ConsumoMedio / 100 * precoLitro;
        }

        
        /// <summary>
        /// Mete combustível no veículo
        /// </summary>
        /// <param name="numLitros"></param>
        /// <remarks>No máximo fica cheio</remarks>
        public void MeteCombustivel(double numLitros)
        {
            double paraEncher = Deposito - Litros;
            Litros += (numLitros > paraEncher) ? paraEncher : numLitros; 

        }

        /// <summary>
        /// Modo textual da classe
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();

            s.Append("--------Dados do Veículo----------");
            s.Append("Matricula: " + Matricula +"\n");
            s.Append("Total em Km's: " + KmTotal + "\n");
            s.Append("Depósito: " + Deposito + "\n");
            s.Append("Litros: " + Litros + "\n");
            s.Append("Consumo Médio: " + ConsumoMedio + "\n");
            s.Append("Viagens Feitas: " + Viagens + "\n");

            return s.ToString();
        }
    }
}
